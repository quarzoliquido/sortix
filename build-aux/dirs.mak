ifeq ($(HOST_IS_SORTIX),1)
  DEFAULT_PREFIX=
  DEFAULT_EXEC_PREFIX=$(PREFIX)
else
  DEFAULT_PREFIX=/usr/local
  DEFAULT_EXEC_PREFIX=$(PREFIX)
endif

PREFIX?=$(DEFAULT_PREFIX)
EXEC_PREFIX?=$(DEFAULT_EXEC_PREFIX)
BINDIR?=$(EXEC_PREFIX)/bin
SBINDIR?=$(EXEC_PREFIX)/sbin
LIBEXECDIR?=$(EXEC_PREFIX)/libexec
BOOTDIR?=$(PREFIX)/boot
DATAROOTDIR?=$(PREFIX)/share
DATADIR?=$(DATAROOTDIR)
SYSCONFDIR?=$(PREFIX)/etc
SHAREDSTATEDIR?=$(PREFIX)/com
LOCALSTATEDIR?=$(PREFIX)/var
RUNSTATEDIR?=$(LOCALSTATEDIR)/run
INCLUDEDIR?=$(PREFIX)/include
DOCDIR?=$(DATAROOTDIR)/doc
INFODIR?=$(DATAROOTDIR)/info
HTMLDIR?=$(DOCDIR)
DVIDIR?=$(DOCDIR)
PSDIR?=$(DOCDIR)
PDFDIR?=$(DOCDIR)
PSDIR?=$(DOCDIR)
LIBDIR?=$(EXEC_PREFIX)/lib
LOCALEDIR?=$(DATAROOTDIR/locale
MANDIR?=$(DATAROOTDIR)/man
MAN1DIR?=$(MANDIR)/man1
MAN2DIR?=$(MANDIR)/man2
MAN3DIR?=$(MANDIR)/man3
MAN4DIR?=$(MANDIR)/man4
MAN5DIR?=$(MANDIR)/man5
MAN6DIR?=$(MANDIR)/man6
MAN7DIR?=$(MANDIR)/man7
MAN8DIR?=$(MANDIR)/man8
MAN9DIR?=$(MANDIR)/man9
MANEXT?=.1
MAN1EXT?=.1
MAN2EXT?=.2
MAN3EXT?=.3
MAN4EXT?=.4
MAN5EXT?=.5
MAN6EXT?=.6
MAN7EXT?=.7
MAN8EXT?=.8
MAN9EXT?=.9

DESTDIR?=
